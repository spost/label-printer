# Label Printer

label-printer is a tool for use in CI that adds some labels to built docker images for tracability.  When invoked as `./label-printer --prefix "com.example.dummyapp" --semver "1.2.3" dummyapp:build`, it will build a new image based off of the specifed image, but with these labels:

- `"com.example.dummyapp.version.full": "1.2.3"`
- `"com.example.dummyapp.version.major": "1"`
- `"com.example.dummyapp.version.minor": "2"`
- `"com.example.dummyapp.version.patch": "3"`

It will also look at the git repo you're running it in (or the one specified with `--repo`) and apply these labels:
- `"com.example.dummyapp.git.branches": "main"`
- `"com.example.dummyapp.git.sha": "1d52c8474d4c6af1298a074611307fbba5839b7a"`

It will then overwrite the old tag to point to this new image.

A complete usage example might look like this:

```sh
export VERSION=1.2.3 # get this from your build artifacts, obviously
docker build --tag dummyapp:build . 
label-printer --prefix "com.example.dummyapp" --semver "$VERSION" dummyapp:build
docker inspect dummyapp:build | jq '.[0].Config.Labels'
{
  "com.example.dummyapp.git.branches": "main",
  "com.example.dummyapp.git.sha": "2d4210378ad71919dec39689fe94ff9b19b049f0",
  "com.example.dummyapp.version.full": "1.2.3",
  "com.example.dummyapp.version.major": "1",
  "com.example.dummyapp.version.minor": "2",
  "com.example.dummyapp.version.patch": "3",
  "label-printer.version.full": "0.5.3",
  "label-printer.version.major": "0",
  "label-printer.version.minor": "5",
  "label-printer.version.patch": "3"
}
docker tag dummyapp:build $REGISTRY/dummyapp:$VERSION
```

Combine with [what-tags](https://gitlab.com/spost/what-tags) for extra gitops:

```sh
docker build --tag dummyapp:build . 
label-printer --prefix "com.example.dummyapp" --semver $VERSION dummyapp:build
what-tags | xargs -I{} docker tag dummyapp:build $REGISTRY/dummyapp:{} 
what-tags | xargs -I{} docker push $REGISTRY/dummyapp:{} 
```
