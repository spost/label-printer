image: clux/muslrust

stages:
  - build
  - test
  - package
  - release

build:
  stage: build
  script:
    - cargo build --release -Z unstable-options --out-dir=.
    - strip label-printer
    - file label-printer | grep 'statically linked'
    - ./label-printer --help
  artifacts:
    paths:
      - label-printer

run tests:
  stage: test
  needs: []
  script:
    - cargo test

package deb:
  image: debian:stable-20210621-slim
  stage: package
  needs:
    - build
  only:
    - tags
  before_script:
    # Don't ask me why this is necessary
    - chmod 0755 deb-build/DEBIAN
    - mkdir -p deb-build/usr/bin
    - cp label-printer deb-build/usr/bin/label-printer
    - export LABEL_PRINTER_VERSION="$(./label-printer --version | cut -d ' ' -f 2)"
    - 'sed --in-place "s/^Version:.*$/Version: $LABEL_PRINTER_VERSION/" deb-build/DEBIAN/control'
  script:
    - cat deb-build/DEBIAN/control
    - dpkg-deb --build deb-build label-printer.deb
  artifacts:
    paths:
      - label-printer.deb

package apk:
  image: alpine:3.14.0
  stage: package
  needs:
    - build
  only:
    - tags
  variables:
    keyfile: "$CI_PROJECT_DIR/apkkey.rsa"
  before_script:
    - apk add --update alpine-sdk
    - echo "$APK_PRIVATE_KEY" | tr '.' '\n' >> $keyfile
    - echo PACKAGER_PRIVKEY="$keyfile" >> /etc/abuild.conf
    - echo REPODEST="$CI_PROJECT_DIR/apk" >> /etc/abuild.conf
    - export LABEL_PRINTER_VERSION="$(./label-printer --version | cut -d ' ' -f 2)"
    - 'sed --in-place "s/^pkgver=.*$/pkgver=$LABEL_PRINTER_VERSION/" apk-build/APKBUILD'
    - tar -cvf apk-build/label-printer.tar label-printer
  script:
    - cat apk-build/APKBUILD
    - cd apk-build
    # We're too lazy to set up non-root users in CI, so just use -F
    - abuild -F checksum && abuild -F -r
    - mv $CI_PROJECT_DIR/apk/label-printer/x86_64/label-printer-$LABEL_PRINTER_VERSION-r0.apk $CI_PROJECT_DIR/label-printer.apk
  artifacts:
    paths:
      - label-printer.apk

release:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: release
  needs:
    - build
    - package apk
    - package deb
  only:
    - tags
  variables:
    WHAT_TAGS_VERSION: v2.2.2
    UPLOAD_PREFIX: "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/generic/label-printer"
    DEB_LOCATION: ${UPLOAD_PREFIX}/${CI_COMMIT_TAG}/label-printer.deb
    APK_LOCATION: ${UPLOAD_PREFIX}/${CI_COMMIT_TAG}/label-printer.apk
    FULL_BINARY_LOCATION: ${UPLOAD_PREFIX}/${CI_COMMIT_TAG}/label-printer
  before_script:
    - apk add --update curl
    - 'wget --header "Authorization: Bearer $CI_JOB_TOKEN" https://gitlab.com/api/v4/projects/23172343/packages/generic/what-tags/$WHAT_TAGS_VERSION/what-tags'
    - chmod +x what-tags
  script:
    - 'curl --fail-with-body --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file label-printer.deb "${DEB_LOCATION}"'
    - 'curl --fail-with-body --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file label-printer.apk "${APK_LOCATION}"'
    - 'curl --fail-with-body --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file label-printer "${FULL_BINARY_LOCATION}"'
    - './what-tags | xargs -I@ curl --fail-with-body --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file label-printer ${UPLOAD_PREFIX}/@/label-printer'
  release:
    name: Release $CI_COMMIT_TAG
    tag_name: $CI_COMMIT_TAG
    ref: $CI_COMMIT_TAG
    description: Release $CI_COMMIT_TAG
    assets:
      links:
        - name: label-printer
          url: $FULL_BINARY_LOCATION
        - name: label-printer.apk
          url: $APK_LOCATION.apk
          link_type: package
        - name: label-printer.deb
          url: $DEB_LOCATION.deb
          link_type: package
