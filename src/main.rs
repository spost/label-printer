use clap::{crate_version, App, Arg};
use futures::join;
use futures::StreamExt;
use git2::{Branch, BranchType, Repository};
use serde::{Deserialize, Serialize};
use shiplift::{BuildOptions, Docker};
use slugify::slugify;
use std::collections::hash_map::DefaultHasher;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::hash::Hasher;
use tempfile::tempdir;

#[derive(Serialize, Deserialize, Debug)]
struct Version {
    major: u64,
    minor: u64,
    patch: u64,
    full: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Git {
    sha: String,
    branches: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
struct Labels {
    git: Git,
    version: Version,
}

impl Labels {
    fn flatten(&self) -> HashMap<&str, String> {
        let mut docker_labels = HashMap::new();
        docker_labels.insert("version.major", self.version.major.to_string());
        docker_labels.insert("version.minor", self.version.minor.to_string());
        docker_labels.insert("version.patch", self.version.patch.to_string());
        docker_labels.insert("version.full", self.version.full.to_string());
        docker_labels.insert("git.sha", self.git.sha.to_string());
        docker_labels.insert("git.branches", self.git.branches.join(","));
        return docker_labels;
    }
}

async fn label_docker_images(
    print_output: bool,
    docker: Docker,
    images: impl Iterator<Item = String>,
    labels: Labels,
    prefix: Option<String>,
    app_version: semver::Version,
) {
    // we work in a temporary directory so we don't pollute the workspace
    let build_dir = tempdir().expect("Failed to create temp directory for building");
    std::env::set_current_dir(build_dir.path())
        .expect("Failed to move to temporary working directory");

    // Take the labels object provided and turn it into a string we can just insert into the file
    let version_labels = labels
        .flatten()
        .iter()
        // LABEL "com.example.version.minor"="[templated value]"
        .map(|(key, value)| {
            vec![
                format!(
                    r#""{prefix}{key}"="{value}""#,
                    prefix = prefix
                        .as_ref()
                        .map_or(String::from(""), |s| format!("{}.", s)),
                    key = key,
                    value = value
                ),
                format!(r#""{key}"="{value}""#, key = key, value = value),
            ]
        })
        .flatten()
        .collect::<Vec<_>>()
        .join(" ");

    let version_env = labels
        .flatten()
        .iter()
        .flat_map(|(key, value)| {
            vec![
                Some(format!(
                    r#""{key}"="{value}""#,
                    key = slugify!(key, separator = "_").to_ascii_uppercase(),
                    value = value
                )),
                prefix.as_ref().map(|s| {
                    format!(
                        r#""{prefix}_{key}"="{value}""#,
                        prefix = slugify!(s, separator = "_").to_ascii_uppercase(),
                        key = slugify!(key, separator = "_").to_ascii_uppercase(),
                        value = value
                    )
                }),
            ]
        })
        .flatten()
        .collect::<Vec<_>>()
        .join(" ");

    let schema_labels = vec![
        format!(
            r#""label-printer.version.full"="{}""#,
            app_version.to_string()
        ),
        format!(
            r#""label-printer.version.major"="{}""#,
            app_version.major.to_string()
        ),
        format!(
            r#""label-printer.version.minor"="{}""#,
            app_version.minor.to_string()
        ),
        format!(
            r#""label-printer.version.patch"="{}""#,
            app_version.patch.to_string()
        ),
    ]
    .join(" ");

    for image_name in images {
        // We don't want to use the image name as the dockerfile name (because it might contain
        // slashes, for instance), so let's just hash it.
        let mut name_hasher = DefaultHasher::new();
        name_hasher.write(&image_name.as_bytes());
        // This just comes out to a bunch of hex
        let dockerfile_name = format!("{:x}", name_hasher.finish());

        let dockerfile_location = &build_dir
            .path()
            .join(&dockerfile_name)
            .with_extension("Dockerfile");

        let dockerfile_content = format!(
            r#"
FROM {image_name}
LABEL {version_labels}
ENV {version_env}
LABEL {schema_labels}
"#,
            image_name = &image_name,
            version_env = version_env,
            version_labels = version_labels,
            schema_labels = schema_labels
        );

        if print_output {
            eprintln!("{}", dockerfile_content);
        }

        fs::write(&dockerfile_location, dockerfile_content).unwrap();

        let build_options = BuildOptions::builder(&build_dir.path().to_str().unwrap().to_string())
            .dockerfile(
                &dockerfile_location
                    .file_name()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .to_string(),
            )
            .tag(&image_name)
            .build();

        let mut stream = docker.images().build(&build_options);
        while let Some(stream_result) = stream.next().await {
            match stream_result {
                Ok(output) => {
                    if let Some(output_stream) = output.get("stream") {
                        output_stream.as_str().map(|string_output| {
                            if print_output {
                                eprint!("{}", string_output);
                            }
                        });
                    } else if let Some(stream_error) = output.get("error") {
                        panic!("Error building docker image! {}", stream_error);
                    }
                }
                Err(e) => panic!("Error building docker image! {}", e),
            }
        }
    }
    let _ = build_dir.close();
}

#[tokio::main]
async fn main() {
    let args = App::new("label-printer")
        .version(crate_version!())
        .author("Sam Osterkil <sam@osterkil.com>")
        .about("Applies labels to specified docker images, based on git information and an optionally-provided semver")
        .arg(Arg::with_name("verbose").long("verbose").short("-v").help("Prints the generated dockerfile(s) and docker build output to stderr."))
        .arg(Arg::with_name("semver").long("semver").takes_value(true).help("The (semantic) version of the docker image being tagged.  Default: 0.0.0."))
        .arg(Arg::with_name("prefix").long("prefix").takes_value(true).help("A prefix to apply to the labels, useful for namespacing."))
        .arg(Arg::with_name("repo_root").long("repo").takes_value(true).help("The root directory of the git repo to analyze.  Default: current directory."))
        .arg(Arg::with_name("images").takes_value(true).multiple(true).required(true).help("The list of docker images to label"))
    .get_matches();

    let verbose = args.is_present("verbose");

    let version = match args.value_of("semver") {
        Some(v) => match v.as_ref() {
            // Allow empty --semver flag, because it simplifies CI pipelines:
            // ... $(if test -n "${VERSION}"; do echo "--semver ${VERSION}"; else echo ""; fi)
            // vs
            // ... --semver "${VERSION}"
            "" => lenient_semver::parse("0.0.0"),
            v => lenient_semver::parse(v),
        },
        None => lenient_semver::parse("0.0.0"),
    }
    .expect("Could not parse provided semver");
    let prefix = args
        .value_of("prefix")
        // Suffix with a dot if provided
        .map(|s| String::from(s));
    let loc = args.value_of("repo_root").unwrap_or(".");
    let images = args.values_of("images").unwrap().map(|s| s.to_string());

    let repo = Repository::open(loc).expect(&format!("Could not open repo at {}", loc));
    let head = repo
        .head()
        .expect("Could not find HEAD?")
        .peel_to_commit()
        .expect("HEAD was not a commit.  You're on your own here, boss.");
    let branches: HashSet<String> = repo
        .branches(None)
        .expect("Could not get a branch list?")
        // unwrap Result<Branch, BranchType, Error>
        .flatten()
        // Filter to branches that point to HEAD
        .filter(|(branch, _): &(Branch, BranchType)| {
            branch
                .get()
                .peel_to_commit()
                .map_or(false, |c| c.id() == head.id())
        })
        .map(|(branch, btype)| {
            let branch_name = branch.name().unwrap().unwrap().to_string();
            match btype {
                // If the branch is local, we good
                BranchType::Local => branch_name,
                // If it's remote, trim off the remote
                BranchType::Remote => branch_name.split_once("/").unwrap().1.to_string(),
            }
        })
        // HEAD will always point to HEAD, obviously - we don't want it
        .filter(|name| name.to_ascii_lowercase() != "head")
        .collect();

    let labels = Labels {
        version: Version {
            major: version.major,
            minor: version.minor,
            patch: version.patch,
            full: version.to_string(),
        },
        git: Git {
            sha: head.id().to_string(),
            branches: branches.into_iter().collect(),
        },
    };

    println!("{}", serde_json::to_string(&labels).unwrap());
    join!(label_docker_images(
        verbose,
        Docker::new(),
        images,
        labels,
        prefix,
        lenient_semver::parse(crate_version!()).unwrap()
    ));
}
